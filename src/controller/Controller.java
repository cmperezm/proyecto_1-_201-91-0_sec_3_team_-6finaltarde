package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;

import model.data_structures.ArregloDinamico;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Quick;
import model.data_structures.Stack;

import model.vo.VOFINEMATPromedio;
import model.vo.VOFecha;
import model.vo.VOInfraccionesPorDireccion;
import model.vo.VOInfraccionesPorHora;
import model.vo.VOMovingViolations;
import model.vo.VOinfraccioesPorCantidadPagada;
import view.MovingViolationsManagerView;

public class Controller {

	private MovingViolationsManagerView view;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> movingViolationsQueue;

	/**
	 * Pila donde se van a cargar los datos de los archivos
	 */
	private IStack<VOMovingViolations> movingViolationsStack;

	private int cuatrimestre;

	private ArregloDinamico<VOMovingViolations> modelo;

	private ArregloDinamico<VOMovingViolations> copy;

	private int totalMovingViolation;

	public Controller() {
		view = new MovingViolationsManagerView();

		totalMovingViolation = 0;
		// TODO, inicializar la pila y la cola
		movingViolationsQueue = null;
		movingViolationsStack = null;
	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		// Controller controller = new Controller();

		while (!fin) {
			view.printMenu();

			int option = sc.nextInt();

			switch (option) {
			case 0:
				view.printMessage("Ingrese el cuatrimestre (1, 2 o 3)");
				int numeroCuatrimestre = sc.nextInt();
				cuatrimestre = numeroCuatrimestre;
				loadMovingViolations(numeroCuatrimestre);
				break;

			case 1:
				verifyObjectIDIsUnique();
				break;

			case 2:

				view.printMessage("Ingrese la fecha con hora inicial (Ej : 2017-03-28T15:30)");
				String fechaInicialReq2A = sc.next();

				view.printMessage("Ingrese la fecha con hora final (Ej : 2017-03-28T15:30)");
				String fechaFinalReq2A = sc.next();

				IQueue<VOFecha> resultados2 = getMovingViolationsInRange(fechaInicialReq2A, fechaFinalReq2A);

				view.printMovingViolationsReq2(resultados2);

				break;

			case 3:

				view.printMessage("Ingrese el VIOLATIONCODE (Ej : T210)");
				String violationCode3 = sc.next();

				avgFineAmountByViolationCode(violationCode3);

				break;

			case 4:

				view.printMessage("Ingrese el ADDRESS_ID");
				String addressId4 = sc.next();

				view.printMessage("Ingrese la fecha con hora inicial (Ej : 2017-03-28)");
				String fechaInicialReq4A = sc.next();

				view.printMessage("Ingrese la fecha con hora final (Ej : 2017-03-28)");
				String fechaFinalReq4A = sc.next();

				IStack<VOInfraccionesPorDireccion> resultados4 = getMovingViolationsAtAddressInRange(addressId4,
						fechaInicialReq4A, fechaFinalReq4A);

				view.printMovingViolationsReq4(resultados4);

				break;

			case 5:
				view.printMessage("Ingrese el limite inferior de FINEAMT  (Ej: 50)");
				double limiteInf5 = sc.nextDouble();

				view.printMessage("Ingrese el limite superior de FINEAMT  (Ej: 50)");
				double limiteSup5 = sc.nextDouble();

				IQueue<VOFINEMATPromedio> violationCodes = violationCodesByFineAmt(limiteInf5, limiteSup5);
				view.printViolationCodesReq5(violationCodes);
				break;

			case 6:

				view.printMessage("Ingrese el limite inferior de TOTALPAID (Ej: 200)");
				double limiteInf6 = sc.nextDouble();

				view.printMessage("Ingrese el limite superior de TOTALPAID (Ej: 200)");
				double limiteSup6 = sc.nextDouble();

				view.printMessage("Ordenar Ascendentmente: (Ej: true)");
				boolean ascendente6 = sc.nextBoolean();

				IStack<VOinfraccioesPorCantidadPagada> resultados6 = getMovingViolationsByTotalPaid(limiteInf6, limiteSup6,
						ascendente6);
				view.printMovingViolationReq6(resultados6);
				break;

			case 7:

				view.printMessage("Ingrese la hora inicial (Ej: 23)");
				int horaInicial7 = sc.nextInt();

				view.printMessage("Ingrese la hora final (Ej: 23)");
				int horaFinal7 = sc.nextInt();

				IQueue<VOInfraccionesPorHora> resultados7 = getMovingViolationsByHour(horaInicial7, horaFinal7);
				view.printMovingViolationsReq7(resultados7);
				break;

			case 8:

				view.printMessage("Ingrese el VIOLATIONCODE (Ej : T210)");
				String violationCode8 = sc.next();

				double[] resultado8 = avgAndStdDevFineAmtOfMovingViolation(violationCode8);

				view.printMessage("FINEAMT promedio: " + resultado8[0] + ", desviación estandar:" + resultado8[1]);
				break;

			case 9:

				view.printMessage("Ingrese la hora inicial (Ej: 23)");
				int horaInicial9 = sc.nextInt();

				view.printMessage("Ingrese la hora final (Ej: 23)");
				int horaFinal9 = sc.nextInt();

				int resultado9 = countMovingViolationsInHourRange(horaInicial9, horaFinal9);

				view.printMessage("Número de infracciones: " + resultado9);
				break;

			case 10:
				printMovingViolationsByHourReq10();
				break;

			case 11:
				view.printMessage("Ingrese la fecha inicial (Ej : 2017-03-28)");
				String fechaInicial11 = sc.next();

				view.printMessage("Ingrese la fecha final (Ej : 2017-03-28)");
				String fechaFinal11 = sc.next();

				double resultados11 = totalDebt(fechaInicial11, fechaFinal11);
				view.printMessage("Deuda total " + resultados11);
				break;

			case 12:
				printTotalDebtbyMonthReq12();

				break;

			case 13:
				fin = true;
				sc.close();
				break;
			}
		}

	}

	/**
	 * Carga los datos de un cuatrimestre
	 */
	public void loadMovingViolations(int cuatrimestre) {

		cuatrimestre--;
		int violationsMonth1 = 0;
		int violationsMonth2 = 0;
		int violationsMonth3 = 0;
		int violationsMonth4 = 0;
		movingViolationsQueue = new Queue<>();
		String line = null;
		for (int i = 1; i <= 4; i++) {

			int mes = i + cuatrimestre * 4;
			File file = new File("Data/infracciones Por Mes/Moving_Violations_Issued_in_Month_" + mes + "_2018.csv");
			try (BufferedReader br = new BufferedReader(new FileReader(file))) {

				br.readLine();
				line = br.readLine();

				while (line != null) {

					String[] data = line.split(",");
					String date = data[13].substring(0, 16);
					int objectId = (int) Double.parseDouble(data[0]);
					int totalPaid = (int) Double.parseDouble(data[9]);
					int finemat = (int) Double.parseDouble(data[8]);
					int penalty1 = 0;
					int penalty2 = 0;
					String accidentIndicator = "";
					String violationCode = "";
					String violationDesc = "";

					if (!data[10].equals("")) {
						penalty1 = Integer.parseInt(data[10]);
					}
					if (!data[11].equals("")) {
						penalty2 = Integer.parseInt(data[11]);
					}
					if (data.length > 15) {
						accidentIndicator = data[12];
						violationCode = data[14];
						violationDesc = data[15];

					}

					VOMovingViolations violation = new VOMovingViolations(objectId, data[3], data[4], finemat,
							totalPaid, penalty1, penalty2, accidentIndicator, date, violationCode, violationDesc);

					movingViolationsQueue.enqueue(violation);
					line = br.readLine();
					totalMovingViolation++;

					if (i == 1) {
						violationsMonth1++;
					} else if (i == 2) {
						violationsMonth2++;
					} else if (i == 3) {
						violationsMonth3++;
					} else if (i == 4) {
						violationsMonth4++;
					}
				}
			} catch (IOException e1) {
				e1.printStackTrace();

			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("LINE " + line);
				String[] data = line.split(",");
				for (int ii = 0; ii < data.length; ii++) {
					System.err.println(" " + ii + ". " + data[ii]);
				}
				throw e;
			}

		}
		if (cuatrimestre == 0) {
			System.out.println("hubo " + violationsMonth1 + " infracciones en el mes de Enero");
			System.out.println("hubo " + violationsMonth2 + " infracciones en el mes de Febrero");
			System.out.println("hubo " + violationsMonth3 + " infracciones en el mes de Marzo");
			System.out.println("hubo " + violationsMonth4 + " infracciones en el mes de Abril");
		} else if (cuatrimestre == 1) {
			System.out.println("hubo " + violationsMonth1 + " infracciones en el mes de Mayo");
			System.out.println("hubo " + violationsMonth2 + " infracciones en el mes de Junio");
			System.out.println("hubo " + violationsMonth3 + " infracciones en el mes de Julio");
			System.out.println("hubo " + violationsMonth4 + " infracciones en el mes de Agosto");
		} else if (cuatrimestre == 2) {
			System.out.println("hubo " + violationsMonth1 + " infracciones en el mes de Septiembre");
			System.out.println("hubo " + violationsMonth2 + " infracciones en el mes de Octubre");
			System.out.println("hubo " + violationsMonth3 + " infracciones en el mes de Noviembre");
			System.out.println("hubo " + violationsMonth4 + " infracciones en el mes de diciembre");
		}

		System.out.println("hubo " + totalMovingViolation + " infracciones en todo el cuatrimestre");
		modelo = new ArregloDinamico<VOMovingViolations>(totalMovingViolation);

		while (!movingViolationsQueue.isEmpty()) {
			modelo.agregar(movingViolationsQueue.dequeue());
		}

		System.out.println("tamano modelo" + modelo.darTamano());
	}

	/**
	 * 1A
	 * 
	 * Verifica si el identificador es unico
	 */
	public void verifyObjectIDIsUnique() {
		boolean repeated = false;

		for (int i = 0; i < modelo.darTamano() - 1; i++) {
			if (modelo.darElemento(i).getObjectId() == modelo.darElemento(i + 1).getObjectId()) {
				repeated = true;
				System.out.println("hay dos infraccion con el mismo id: " + modelo.darElemento(i).getObjectId());
			}
		}
		if (!repeated) {
			System.out.println("No hay infracciones duplicadas");
		}
	}

	/**
	 * 2A
	 * 
	 * Consultar infracciones por fecha/hora inicial y fecha/hora final
	 */
	public IQueue<VOFecha> getMovingViolationsInRange(String inicial, String fechaFinal) {
		IQueue<VOFecha> queue = new Queue<>();
		for (int i = 0; i < modelo.darTamano(); i++) {

			VOMovingViolations element = modelo.darElemento(i);
			if (element.getTicketIssueDate().compareTo(inicial) >= 0
					&& element.getTicketIssueDate().compareTo(fechaFinal) <= 0) {

				VOFecha movingViolation = new VOFecha(element.getObjectId(), element.getTicketIssueDate());
				queue.enqueue(movingViolation);
			}
		}
		return queue;
	}

	/**
	 * 3A
	 * 
	 * Dado un tipo de infracción (VIOLATIONCODE) informar el (FINEAMT)promedio
	 * cuando no hubo accidente y el (FINEAMT) promedio cuando si lo hubo.
	 */
	public void avgFineAmountByViolationCode(String code) {
		double accident = 0;
		double noAccident = 0;
		double countAccident = 0;
		double countNoAccident = 0;

		for (int i = 0; i < modelo.darTamano(); i++) {
			if (modelo.darElemento(i).getViolationCode().equals(code)) {
				if (modelo.darElemento(i).getAccidentIndicator().equals("No")) {
					noAccident = noAccident + modelo.darElemento(i).getFinemat();
					countNoAccident++;
				} else if (modelo.darElemento(i).getAccidentIndicator().equals("Yes")) {
					accident = accident + modelo.darElemento(i).getFinemat();
					countAccident++;
				}
			}
		}
		System.out.println("El finemat promedio cuando SI hubo accidentes con el codigo " + code + " fue de: "
				+ accident / countAccident);
		System.out.println("El finemat promedio cuando NO hubo accidentes con el codigo " + code + " fue de: "
				+ noAccident / countNoAccident);
	}

	/**
	 * 4A.
	 * 
	 * Consultar las infracciones en una dirección (ADDRESS_ID) elrango fechainicial
	 * y fecha final.Ordenar descendentemente porSTREETSEGID y fecha.
	 */
	public IStack<VOInfraccionesPorDireccion> getMovingViolationsAtAddressInRange(String addresId, String inicial,
			String fechaFinal) {
		
		ArregloDinamico<VOInfraccionesPorDireccion> arregloDesordenado = new ArregloDinamico<>(20);
		for (int i = 0; i < modelo.darTamano(); i++) {

			VOMovingViolations element = modelo.darElemento(i);

			if ((element.getTicketIssueDate().compareTo(inicial)) >= 0
					&& (element.getTicketIssueDate().compareTo(fechaFinal)) <= 0
					&& element.getAddresId().equals(addresId)) {

				VOInfraccionesPorDireccion movingViolation = new VOInfraccionesPorDireccion(element.getObjectId(),
						element.getTicketIssueDate(), element.getStreetsegid(), element.getAddresId());
				arregloDesordenado.agregar(movingViolation);
			}
		
			
		}
		Comparable[] arr = arregloDesordenado.getCopy();
		Quick.sort(arr, 0, arr.length-1);
		IStack<VOInfraccionesPorDireccion> stack = new Stack<>();
		for (int i = 0; i < arr.length; i++) {
			stack.push((VOInfraccionesPorDireccion) arr[i]);
		}
		return stack;
	}

	/**
	 * 1B Consultar los tipos de infracciones (VIOLATIONCODE) con su valor (FINEAMT)
	 * promedio en un rango dado.
	 * 
	 * @return
	 */
	public IQueue<VOFINEMATPromedio> getFinematpromedio(int finematInicial, int finematFinal) {
		IQueue<VOFINEMATPromedio> cola = new Queue<>();

		int contador = 0;
		int Finemat = 0;

		for (int i = 0; i < modelo.darTamano(); i++) {
			VOMovingViolations element = modelo.darElemento(i);

			if ((element.getFinemat() > (finematInicial)) && (element.getFinemat() < finematFinal)) {
				String vcode = element.getViolationCode();
				Finemat = element.getFinemat();
				contador++;
				int promedio = Finemat / contador;

				VOFINEMATPromedio movingViolation = new VOFINEMATPromedio(vcode, promedio);
				cola.enqueue(movingViolation);
			}
		}
		return cola;
	}

	/**
	 * 2B Consultar infracciones donde la cantidad pagada (TOTALPAID) esta en un
	 * rango dado. Se ordena por fecha de infracción.
	 * 
	 */

	public IStack<VOinfraccioesPorCantidadPagada> getMovingViolationsByTotalPaid(double totalInicial, double totalFinal,
			boolean ordenA) {
		IStack<VOinfraccioesPorCantidadPagada> pila = new Stack<>();
		if (ordenA = true) {
			for (int i = 0; i < modelo.darTamano(); i++) {
				VOMovingViolations element = modelo.darElemento(i);
				VOMovingViolations menor = null;
				if (element.getTotalPaid() > totalInicial && element.getTotalPaid() < totalFinal) {
					if (element.getTicketIssueDate().compareTo(menor.getTicketIssueDate()) < 0) {
						menor = element;
					}

				}
				// Se agregan los párametros del menor.
				VOinfraccioesPorCantidadPagada movingViolation = new VOinfraccioesPorCantidadPagada();
				pila.push(movingViolation);
			}
		}

		else {
			for (int i = 0; i < modelo.darTamano(); i++) {
				VOMovingViolations element = modelo.darElemento(i);
				VOMovingViolations mayor = null;
				if (element.getTotalPaid() > totalInicial && element.getTotalPaid() < totalFinal) {
					if (element.getTicketIssueDate().compareTo(mayor.getTicketIssueDate()) > 0) {
						mayor = element;
					}

				}
				// Se agregan los párametros del mayor.
				VOinfraccioesPorCantidadPagada movingViolation = new VOinfraccioesPorCantidadPagada();
				pila.push(movingViolation);
			}
		}

		return pila;
	}

	/**
	 * 3B Consultar infracciones por hora inicial y hora final, ordenada
	 * ascendentemente por VIOLATIONDESC
	 * 
	 * @return
	 */
	public IQueue<VOInfraccionesPorHora> getMovingViolationsByHour(int horaInicial, int horaFinal) {

		IQueue<VOInfraccionesPorHora> cola = new Queue<>();

		for (int i = 0; i < modelo.darTamano(); i++) {
			VOMovingViolations element = modelo.darElemento(i);

		}

		return cola;
	}

	/**
	 * 4B Dado un tipo de infracción (VIOLATIONCODE) informar el (FINEAMT) promedio
	 * y su desviación estándar.
	 * 
	 * @param code
	 * @return
	 */
	public double[] avgAndStdDevFineAmtOfMovingViolation(String code) {
		int contador = 0;
		int FINEMAT = 0;
		int promedio = 0;
		double desviacion = 0;
		int sum = 0;
		double[] resultados = null;
		for (int i = 0; i < modelo.darTamano(); i++) {
			VOMovingViolations element = modelo.darElemento(i);
			if (element.getViolationCode().compareToIgnoreCase(code) == 0) {
				FINEMAT += element.getFinemat();
				contador++;

			}
			promedio = FINEMAT / contador;
			sum += Math.pow(FINEMAT - promedio, 2);
			desviacion = Math.sqrt(sum / FINEMAT);
		}

		resultados[0] = promedio;
		resultados[1] = desviacion;

		return resultados;
	}

	/**
	 * 1C.
	 * 
	 * El número de infracciones que ocurrieron en un rango de horas del día. Se
	 * define el rango de horas por valores enteros en el rango [0, 24].
	 */
	private int countMovingViolationsInHourRange(int horaInicial9, int horaFinal9) {
		int count = 0;

		for (int i = 0; i < modelo.darTamano(); i++) {

			int date = Integer.parseInt(modelo.darElemento(i).getTicketIssueDate().substring(11, 13));

			if (date >= horaInicial9 && date <= horaFinal9) {
				count++;
			}
		}
		return count;
	}

	/**
	 * 2C.
	 * 
	 * Grafica ASCII con el porcentaje de infracciones que tuvieron accidentes por
	 * horadel día.
	 */
	public void printMovingViolationsByHourReq10() {

		int[] graphic = new int[24];
		int total = 0;
		System.out.println(" MODELO " + modelo.darTamano());
		for (int i = 0; i < modelo.darTamano(); i++) {
			if (modelo.darElemento(i).getAccidentIndicator().equals("Yes")) {
				int hour = Integer.parseInt(modelo.darElemento(i).getTicketIssueDate().substring(11, 13));
				graphic[hour] = graphic[hour] + 1;
				total++;
			}
		}
		paintGraphic(total, graphic);
	}

	/**
	 * Dibuja la grafica.
	 */
	public void paintGraphic(int total, int[] values) {

		System.out.println("Hora| % de accidentes");

		for (int i = 0; i < values.length; i++) {

			int percentage = (int) Math.round((values[i] * 100.0) / total);
			String x = "";

			for (int j = 1; j <= percentage; j++) {
				x = x + "x";
			}

			String zero = "";
			if (i < 10) {
				zero = "0";
			}
			System.out.println(zero + i + "  |  " + x);
		}
		System.out.println("\n" + "cada x es igual a 1%");
	}

	/**
	 * 3C.
	 * 
	 * La deuda (TOTALPAID –FINEAMT -PENALTY1 –PENALTY2) total por infracciones que
	 * se dieron en un rango de fechas.
	 */
	private double totalDebt(String fechaInicial11, String fechaFinal11) {

		double totalDebt = 0;
		for (int i = 0; i < totalMovingViolation; i++) {

			VOMovingViolations element = modelo.darElemento(i);

			if (element.getTicketIssueDate().compareTo(fechaInicial11) >= 0
					&& element.getTicketIssueDate().compareTo(fechaFinal11) <= 0) {

				double Debt = element.getFinemat() + element.getPenalty1() + element.getPenalty2()
						- element.getTotalPaid();
				totalDebt = totalDebt + Debt;

			}
		}
		return totalDebt;
	}

	/**
	 * 4C.
	 * 
	 * Grafica ASCIIconla deuda acumulada totalpor infracciones.
	 */
	public void printTotalDebtbyMonthReq12() {

		double totalDebt = 0;
		System.out.println("Mes | Dinero");
		for (int i = 1; i <= 4; i++) {
			String zero = "";
			int mes = i + (cuatrimestre - 1) * 4;
			if (mes < 10) {
				zero = "0";
			}

			totalDebt = totalDebt+ totalDebt("2018-" + zero + mes + "-01", "2018-" + zero + (mes + 1) + "-01T00:00");
			double totalDebt2 = totalDebt / 1000000;
			String x = "";

			for (int j = 1; j < totalDebt2; j++) {
				x = x + "x";
			}
			System.out.println("0" + i + "  |  " + x);
		
		}

		System.out.println("cada X es igual a 1000.000$");

	}

	/**
	 * 1B Consultar los tipos de infracciones (VIOLATIONCODE) con su valor (FINEAMT)
	 * promedio en un rango dado.
	 * 
	 * @return
	 */
	public IQueue<VOFINEMATPromedio> violationCodesByFineAmt(double finematInicial, double finematFinal) {
		IQueue<VOFINEMATPromedio> cola = new Queue<>();

		int contador = 0;
		int Finemat = 0;

		for (int i = 0; i < modelo.darTamano(); i++) {
			VOMovingViolations element = modelo.darElemento(i);

			if ((element.getFinemat() > (finematInicial)) && (element.getFinemat() < finematFinal)) {
				String vcode = element.getViolationCode();
				Finemat = element.getFinemat();
				contador++;
				int promedio = Finemat / contador;

				VOFINEMATPromedio movingViolation = new VOFINEMATPromedio(vcode, promedio);
				cola.enqueue(movingViolation);
			}
		}
		return cola;
	}

	/**
	 * 2B Consultar infracciones donde la cantidad pagada (TOTALPAID) esta en un
	 * rango dado. Se ordena por fecha de infracción.
	 * 
	 */

	public IStack<VOinfraccioesPorCantidadPagada> getinfraccioesPorCantidadPagada(int totalInicial, int totalFinal,
			boolean ordenA) {
		IStack<VOinfraccioesPorCantidadPagada> pila = new Stack<>();
		if (ordenA = true) {
			for (int i = 0; i < modelo.darTamano(); i++) {
				VOMovingViolations element = modelo.darElemento(i);
				VOMovingViolations menor = null;
				if (element.getTotalPaid() > totalInicial && element.getTotalPaid() < totalFinal) {
					if (element.getTicketIssueDate().compareTo(menor.getTicketIssueDate()) < 0) {
						menor = element;
					}

				}
				// Se agregan los párametros del menor.
				VOinfraccioesPorCantidadPagada movingViolation = new VOinfraccioesPorCantidadPagada();
				pila.push(movingViolation);
			}
		}

		else {
			for (int i = 0; i < modelo.darTamano(); i++) {
				VOMovingViolations element = modelo.darElemento(i);
				VOMovingViolations mayor = null;
				if (element.getTotalPaid() > totalInicial && element.getTotalPaid() < totalFinal) {
					if (element.getTicketIssueDate().compareTo(mayor.getTicketIssueDate()) > 0) {
						mayor = element;
					}

				}
				// Se agregan los párametros del mayor.
				VOinfraccioesPorCantidadPagada movingViolation = new VOinfraccioesPorCantidadPagada();
				pila.push(movingViolation);
			}
		}

		return pila;
	}

	/**
	 * 3B Consultar infracciones por hora inicial y hora final, ordenada
	 * ascendentemente por VIOLATIONDESC
	 * 
	 * @return
	 */
	public IQueue<VOInfraccionesPorHora> getVOInfraccionesPorHora(int horaInicial, int horaFinal) {

		IQueue<VOInfraccionesPorHora> cola = new Queue<>();

		for (int i = 0; i < modelo.darTamano(); i++) {
			VOMovingViolations element = modelo.darElemento(i);

		}

		return cola;
	}

	/**
	 * 4B Dado un tipo de infracción (VIOLATIONCODE) informar el (FINEAMT) promedio
	 * y su desviación estándar.
	 * 
	 * @param code
	 * @return
	 */
	public double[] getVOMovingViolations(String code) {
		int contador = 0;
		int FINEMAT = 0;
		int promedio = 0;
		double desviacion = 0;
		int sum = 0;
		double[] resultados = null;
		for (int i = 0; i < modelo.darTamano(); i++) {
			VOMovingViolations element = modelo.darElemento(i);
			if (element.getViolationCode().compareToIgnoreCase(code) == 0) {
				FINEMAT += element.getFinemat();
				contador++;

			}
			promedio = FINEMAT / contador;
			sum += Math.pow(FINEMAT - promedio, 2);
			desviacion = Math.sqrt(sum / FINEMAT);
		}

		resultados[0] = promedio;
		resultados[1] = desviacion;

		return resultados;
	}

}
