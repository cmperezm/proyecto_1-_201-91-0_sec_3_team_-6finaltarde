package model.data_structures;

/**
 * 2019-01-23 Estructura de Datos Arreglo Dinamico de Strings. El arreglo al
 * llenarse (llegar a su maxima capacidad) debe aumentar su capacidad.
 * 
 * @author Fernando De la Rosa
 *
 */
public class ArregloDinamico<T extends Comparable<T>> implements IArregloDinamico<T> {
	/**
	 * Capacidad maxima del arreglo
	 */
	private int tamanoMax;
	/**
	 * Numero de elementos en el arreglo (de forma compacta desde la posicion 0)
	 */
	private int tamanoAct;
	/**
	 * Arreglo de elementos de tamaNo maximo
	 */
	private Object elementos[];

	/**
	 * Construir un arreglo con la capacidad maxima inicial.
	 * 
	 * @param max Capacidad maxima inicial
	 */
	public ArregloDinamico(int max) {
		elementos = new Object[max];
		tamanoMax = max;
		tamanoAct = 0;
	}

	public void agregar(T dato) {
		if (dato == null) {
			throw new NullPointerException("fuck");
		}
		if (tamanoAct == tamanoMax) { // caso de arreglo lleno (aumentar tamaNo)
			tamanoMax = 2 * tamanoMax;
			Object[] copia = elementos;
			elementos = new Object[tamanoMax];
			for (int i = 0; i < tamanoAct; i++) {
				elementos[i] = copia[i];
			}
			System.out.println("Arreglo lleno: " + tamanoAct + " - Arreglo duplicado: " + tamanoMax);
		}
		elementos[tamanoAct] = dato;
		tamanoAct++;
	}

	@Override
	public int darTamano() {
		return tamanoAct;
	}

	@Override
	public T darElemento(int i) {
		return (T) elementos[i];
	}

	@Override
	public T buscar(T dato) {

		for (int i = 0; i < tamanoAct; i++) {

			int x = dato.compareTo((T) darElemento(i));
			if (x == 0) {
				return darElemento(i);
			}
		}
		return null;
	}

	@Override
	public T eliminar(T dato) {

		for (int i = 0; i < tamanoAct; i++) {

			int x = dato.compareTo((T) darElemento(i));
			if (x == 0) {
				T g = (T) elementos[i];
				for (int j = i; j < tamanoAct; j++) {
					elementos[j] = elementos[j + 1];
				}
				return g;
			}
		}
		return null;
	}
	
	public Comparable[] getCopy() {
		Comparable[] resp = new Comparable[darTamano()];
		for (int i = 0; i < resp.length; i++) {
			resp[i] = (Comparable) elementos[i];
		}
		return resp;
	}
}
