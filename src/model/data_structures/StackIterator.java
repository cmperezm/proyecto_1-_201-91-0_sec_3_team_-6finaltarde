package model.data_structures;

import java.util.Iterator;

public class StackIterator<T> implements Iterator<T>{

	private Node<T> p;

	public StackIterator(Node<T> p) {
		this.p = p;
	}

	@Override
	public boolean hasNext() {
		// 
		return  p != null;
	}

	@Override
	public T next() {
		T data = p.getData();
		p = p.getNext();
		return data;
	}



}
