package model.data_structures;

import java.util.Iterator;

public class Stack <T>implements IStack<T> {

	private Node<T> head;

	public Stack() {
		head = null;
	}

	@Override
	public Iterator<T> iterator() {
		return new StackIterator<T>(head);

	}

	@Override
	public boolean isEmpty() {
		return head == null;
	}

	@Override
	public int size() {
		int count = 0;
		Node<T> p = head;
		while (p != null) {
			count++	;
			p = p.getNext();
		}
		return count;
	}

	@Override
	public void push(T t) {
		Node<T> p = head;

		Node<T> node = new Node<T>(t);
		head = node;
		node.setNext(p);
	}

	@Override
	public T pop() {

		Node<T> p = head;
		head = head.getNext();
		p.setNext(null);
		return p.getData();
	}





}

