package model.data_structures;

import java.util.Iterator;

public class Queue<T> implements IQueue<T>{

	private Node<T> head;
	
	private Node<T> tail;
	
	public Queue() {
		head = null;
		tail = null;
	}
	@Override
	public Iterator<T> iterator() {
		return new StackIterator<T>(head); 
	}

	@Override
	public boolean isEmpty() {
		// TODO
		return head == null;
	}

	@Override
	public int size() {
		int count = 0;
		Node<T> p = head;
		while (p != null) {
			count++	;
			p = p.getNext();
		}
		return count;
	}

	@Override
	public void enqueue(T t) {
		Node<T> node = new Node<T>(t);		
		
		
		if(isEmpty()) {
			head = node;
			tail = node;
			return;
		}		
		tail.setNext(node);
		tail = node;
	}

	@Override
	public T dequeue() {
		if(isEmpty()) {
			return null;
		}
		T resp = head.getData();
		if(head.getNext() == null) {
			head = null; 
			tail = null;
			return resp;
		}
		Node<T> p = head;
		head = head.getNext();
		p.setNext(null);
		return resp;
	}

}
