package model.vo;

public class VOPromedioFinematInfraccion {

	private int conAccidente;
	
	private int sinAccidente;

	public VOPromedioFinematInfraccion(int conAccidente, int sinAccidente) {
		super();
		this.conAccidente = conAccidente;
		this.sinAccidente = sinAccidente;
	}

	public int getConAccidente() {
		return conAccidente;
	}

	public void setConAccidente(int conAccidente) {
		this.conAccidente = conAccidente;
	}

	public int getSinAccidente() {
		return sinAccidente;
	}

	public void setSinAccidente(int sinAccidente) {
		this.sinAccidente = sinAccidente;
	}
	
	
}
