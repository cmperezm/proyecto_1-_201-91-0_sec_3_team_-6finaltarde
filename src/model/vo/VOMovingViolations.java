package model.vo;

/**
 * Representation of a Trip object
 * @param <T>
 */
public class VOMovingViolations implements Comparable<VOMovingViolations> {

	private int objectId;
	
	private String addresId;

	private String streetsegid;
		
	private int finemat;
	
	private int totalPaid;
	
	private int penalty1;

	private int penalty2;
	
	private String accidentIndicator;
	
	private String ticketIssueDate;

	private String violationCode;
	
	 private String violationDesc;


	public VOMovingViolations(int objectId, String addresId, String streetsegid, int finemat, int totalPaid,
			int penalty1, int penalty2, String accidentIndicator, String ticketIssueDate, String violationCode, String violationDesc) {
		super();
		this.objectId = objectId;
		this.addresId = addresId;
		this.streetsegid = streetsegid;
		this.finemat = finemat;
		this.totalPaid = totalPaid;
		this.penalty1 = penalty1;
		this.penalty2 = penalty2;
		this.accidentIndicator = accidentIndicator;
		this.ticketIssueDate = ticketIssueDate;
		this.violationCode = violationCode;
		this.violationDesc = violationDesc;
	}


	public int getObjectId() {
		return objectId;
	}


	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}


	public String getAddresId() {
		return addresId;
	}


	public void setAddresId(String addresId) {
		this.addresId = addresId;
	}


	public String getStreetsegid() {
		return streetsegid;
	}


	public void setStreetsegid(String streetsegid) {
		this.streetsegid = streetsegid;
	}


	public int getFinemat() {
		return finemat;
	}


	public void setFinemat(int finemat) {
		this.finemat = finemat;
	}


	public int getTotalPaid() {
		return totalPaid;
	}


	public void setTotalPaid(int totalPaid) {
		this.totalPaid = totalPaid;
	}


	public int getPenalty1() {
		return penalty1;
	}


	public void setPenalty1(int penalty1) {
		this.penalty1 = penalty1;
	}


	public int getPenalty2() {
		return penalty2;
	}


	public void setPenalty2(int penalty2) {
		this.penalty2 = penalty2;
	}


	public String getAccidentIndicator() {
		return accidentIndicator;
	}


	public void setAccidentIndicator(String accidentIndicator) {
		this.accidentIndicator = accidentIndicator;
	}


	public String getTicketIssueDate() {
		return ticketIssueDate;
	}


	public void setTicketIssueDate(String ticketIssueDate) {
		this.ticketIssueDate = ticketIssueDate;
	}


	public String getViolationCode() {
		return violationCode;
	}


	public void setViolationCode(String violationCode) {
		this.violationCode = violationCode;
	}


	@Override
	public int compareTo(VOMovingViolations o) {
		// TODO Auto-generated method stub
		return 0;
	}

@Override
public String toString() {

	return ""+objectId+" "+ticketIssueDate;
}


	
	
}
