package model.vo;

public class VOFINEMATPromedio {

	private String violationCode;
	
	private int finemat;

	public VOFINEMATPromedio(String violationCode, int finemat) {
		super();
		this.violationCode = violationCode;
		this.finemat = finemat;
	}

	public String getViolationCode() {
		return violationCode;
	}

	public void setViolationCode(String violationCode) {
		this.violationCode = violationCode;
	}

	public int getFinemat() {
		return finemat;
	}

	public void setFinemat(int finemat) {
		this.finemat = finemat;
	}
	
	
}
