package model.vo;

public class VOInfraccionesPorDireccion implements Comparable<VOInfraccionesPorDireccion>{

	private int objectId;
	
	private String ticketIssueDate; 

	private String streetsegid;
	
	private String addresId;
	
	
	public VOInfraccionesPorDireccion(int objectId, String ticketIssueDate, String streetsegid, String addresId) {
		super();
		this.objectId = objectId;
		this.ticketIssueDate = ticketIssueDate;
		this.streetsegid = streetsegid;
		this.addresId = addresId;
	}


	public int objectId() {
		return objectId;
	}


	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}


	public String getTicketIssueDate() {
		return ticketIssueDate;
	}


	public void setTicketIssueDate(String ticketIssueDate) {
		this.ticketIssueDate = ticketIssueDate;
	}


	public String getStreetSegId() {
		return streetsegid;
	}


	public void setStreetsegid(String streetsegid) {
		this.streetsegid = streetsegid;
	}


	public String getAddressId() {
		return addresId;
	}


	public void setAddresId(String addresId) {
		this.addresId = addresId;
	}


	@Override
	public int compareTo(VOInfraccionesPorDireccion o) {
		
	
		int x = streetsegid.trim().compareTo( o.getStreetSegId().trim());
		if(x == 0 )
		{
			
			x=ticketIssueDate.trim().compareTo(o.getTicketIssueDate().trim());
	
		}
		return ticketIssueDate.compareTo(o.getTicketIssueDate());
	}
	
	
}
