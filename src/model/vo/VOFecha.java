package model.vo;

public class VOFecha implements Comparable<VOFecha> {

	private int id;
	
	private String ticketIssueDate;

	public VOFecha(int id, String ticketIssueDate) {
		super();
		this.id = id;
		this.ticketIssueDate = ticketIssueDate;
	}

	public int objectId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTicketIssueDate() {
		return ticketIssueDate;
	}

	public void setTicketIssueDate(String ticketIssueDate) {
		this.ticketIssueDate = ticketIssueDate;
	}

	@Override
	public int compareTo(VOFecha o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
